% Compute the information rate (in nats) according to different methods
% Channel: AWGN with nonlinear phase noise (can be turned off)
% Input distribution: complex Gaussian
%
% Nonlinear phase noise can be turned off by setting gm0=0
%
% Computation methods:
% - exact (Shannon capacity)
% - auxiliary channel lower bound with AWGN metrics (MIapp1)
% - Average nearest neighbor distance (MIapp2 and MIapp3)

% <<<Note:given (x:y:z) increasing z above 154 while keeping x=0 and y=5 lead the AIR to explode

%for j=Ntot:4000:4000
SNR=(0:10:20);
gm0=0;
%gm0=0.001;             % nonlinear parameter
Ntot=50;               % number of samples
Nit=length(SNR);
MIapp1=zeros(1,Nit);
MIapp2=zeros(1,Nit);
MIapp3=zeros(1,Nit);
MIapp4=zeros(1,Nit);
MIapp5=zeros(1,Nit);
MIapp6=zeros(1,Nit);
MIapp7=zeros(1,Nit);
volume=zeros(1,Nit);
MIsh=zeros(1,Nit);
V1=zeros(1,Nit);

p = gcp('nocreate'); % If no pool, do not create new one.
if isempty(p)
    parpool(5);
else
    if ( p.NumWorkers ~= 5 )
        delete(gcp);
        parpool(5);
    end
end

bar=waitbar(0,'Computing..');
for i=1:Nit,
    real=0;
    N=Ntot;
    SNRlin=10.0^(0.1*SNR(i));
    gm=gm0*SNRlin;
    
    sg2x = 1.0;                         % varianza (totale) di x
    sg2n = sg2x/SNRlin;                 % varianza (totale) di n
    sg2y = sg2x^2+sg2n^2;               % varianza (totale) di y
    
    if real
        x = sqrt(0.5*sg2x)*randn(N,1);         % i.i.d. input samples
        n = sqrt(0.5*sg2n)*randn(N,1);
        % i.i.d. noise samples
    else
        x = sqrt(0.5*sg2x)*(randn(N,1)+1i*randn(N,1));         % i.i.d. input samples
        n = sqrt(0.5*sg2n)*(randn(N,1)+1i*randn(N,1));
    end
    y = (x+n).*exp(-1i*gm*abs(x+n).^2);
    
    sg2xapp=sum(abs(x).^2)/(N-1);
    sg2yapp=sum(abs(y).^2)/(N-1);
    sgxyapp=sum(x.*conj(y))/(N-1);
    % A=abs(sgxyapp)^2/(sg2xapp*sg2yapp-abs(sgxyapp)^2);
    h=sgxyapp/sg2xapp;
    z=y./h;
    
    %  xy = 1/sqrt(2*pi*sg2n) * exp(-power( (bsxfun(@minus , y , x)),2)/2*sg2n^2) used for conditional MI(X;Y|channel);
    
    %     MIapp6(i) = log(1+A)-A/(N*log(2))*sum(abs(z-x).^2-(abs(z).^2)./(A+1));
    %  MIapp2(i)=mutual_information(x,y,0);
    %  MIapp3(i)=mutual_information(x,y,2);
    a=zeros(5,1);
    for j=1:5
        %tic
        V=0;
        if j==10
            a(j) = kNN_Kraskov(x , y , 4 );
            %    sprintf('finished %d: %f',j,toc)
        end
        if j==20
         %   a(j) = kNN_Kraskov(x,y,4,'Rectangle',0);
            % sprintf('finished %d: %f',j,toc)
        end
        if j==30
            [ a(j) , V ] = kNN_Kraskov(x,y,4,'Cube',1);
            %sprintf('finished %d: %f',j,toc)
        end
        if j==40
          %  a(j) = kNN_Kraskov(x,y,4,'Rectangle',1);
            % sprintf('finished %d: %f',j,toc)
        end
        %   if j==5
        %a(j) = ( ( MultiKraskov(x,y,xy, 4,'Cube',0))) ;%+ kNN_Kraskov(x,y,4,'Cube',0) ;
        %       a(j) = 0;
        %   end
        if j==5
            a(j) = MILocalGaussian(x,y,1);
            %sprintf('finished %d: %f',j,toc)
        end
    end
    MIapp1(i)=a(1);
    MIapp2(i) =a(2);
    MIapp3(i)=a(3);
    MIapp4(i)=a(4);
    MIapp5(i)=a(5);
    MIsh(i) = log(1+SNRlin);
waitbar(i/Nit);
end
close(bar);
figure
hold on
plot(SNR, MIapp1 , 'black--o');
%plot(SNR, MIapp2 ,'--red','LineWidth',2,'MarkerSize',4,'MarkerEdgeColor','b', 'MarkerFaceColor',[0.5,0.5,0.5]);
plot(SNR, MIapp3 , 'green');
%plot(SNR, MIapp4 , 'magenta');
plot(SNR, MIapp5 , 'cyan');
plot(SNR, MIapp6 , 'red');
plot(SNR , MIsh,  'p:')
figure
plot(SNR, MIapp5 , 'red');
