IN1 = randn(1000,1);
OUT1 = randn(1000,1);

IN2 = randn(1000,2);
OUT2= randn(1000,2);

IN3 = exp(1i*pi*randn(30,1));
OUT3 = exp(1i*pi*randn(30,1));

IN4 = exp(1i*pi*randn(1000,10));
OUT4 = exp(1i*pi*randn(1000,10));

IN5 = randn(50,2);
OUT5 = randn(50,2);

IN0 = [ 1; 2 ; 3 ; 4 ; 5 ];
OUT0 = [ 6 ; 7 ; 8 ; 9 ; 10 ];
dx=2;
DistanceMetric = @(X,Y)(transpose(max(transpose(pdist2(X(:,1:dx),Y(:,1:dx),'euclidean')),transpose(pdist2(X(:,dx+1:2*dx),Y(:,dx+1:2*dx),'euclidean')))));


%     ezsurf(fun) TO 3d function print


%Good values repeat = 10 epsilon=0.7