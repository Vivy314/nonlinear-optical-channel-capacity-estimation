% Compute the information rate (in nats) according to different methods 
% Channel: AWGN with nonlinear phase noise (can be turned off)
% Input distribution: complex Gaussian
%
% Nonlinear phase noise can be turned off by setting gm0=0
%

% Computation methods:
% - exact (Shannon capacity)
% - auxiliary channel lower bound with AWGN metrics (MIapp1)
% - Average nearest neighbor distance (MIapp2 and MIapp3)


%probably.. assuming AWGN channel, increasing SNR= [0,5,10,15,20,25,30,35] check how MI change 



SNR=(0:5:35);
gm0=0;
% gm0=0.001;             % nonlinear parameter
Ntot=1000;               % number of samples
Nit=length(SNR);
MIapp1=zeros(1,Nit);
MIapp2=zeros(1,Nit);
MIapp3=zeros(1,Nit);
MIsh=zeros(1,Nit);

for i=1:Nit,
    N=Ntot;
    SNRlin=10.0^(0.1*SNR(i));
    gm=gm0*SNRlin;
    sg2x = 1.0;                         % varianza (totale) di x
    sg2n = sg2x/SNRlin;                 % varianza (totale) di n
    sg2y = sg2x^2+sg2n^2;               % varianza (totale) di y
    
    
    % N complex random values time 1/sqrt(2)
    x = sqrt(0.5*sg2x)*(randn(N,1)+1i*randn(N,1));         % i.i.d. input samples
    n = sqrt(0.5*sg2n)*(randn(N,1)+1i*randn(N,1));         % i.i.d. noise samples
    y = (x+n).*exp(-1i*gm*abs(x+n).^2);                    % channel

    sg2xapp=sum(abs(x).^2)/(N-1);
    sg2yapp=sum(abs(y).^2)/(N-1);
    sgxyapp=sum(x.*conj(y))/(N-1);
    A=abs(sgxyapp)^2/(sg2xapp*sg2yapp-abs(sgxyapp)^2);
    h=sgxyapp/sg2xapp;
    z=y./h;
      
    % should be AIR implementation
    MIapp1(i) = log(1+A)-A/(N*log(2))*sum(abs(z-x).^2-(abs(z).^2)./(A+1));
    %chebychev distance (better than MIapp3)
    MIapp2(i)=mutual_information(x,y,0);
    %Euclidean distance
    MIapp3(i)=mutual_information(x,y,2);
       
    MIsh(i) = log(1+SNRlin);
    
end

plot(MIapp1)
plot(MIapp2)
plot(MIapp3)
plot(MIsh)