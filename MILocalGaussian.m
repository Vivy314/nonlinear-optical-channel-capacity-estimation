
function [ MI ] = MILocalGaussian( x , y , Optim )
    
    if nargin < 2 || nargin > 3
        error('Too many input arguments.');
    end
    
    if nargin < 2
        error('Not enough input arguments.');
    end
    if nargin == 2
        Optim = false;
    end
    
    
    [sizex,dx] = size(x);
    [sizey, dy ] = size(y);
    
    if sizex ~= sizey
        error('Vecotor size has to be the same');
    end
    
    % Structuring complex numbers
    if ~isreal(x)
        x= [ real(x),imag(x) ];
        dx=2*dx;
    end
    if ~isreal(y)
        y = [ real(y), imag(y)];
        dy=dy*2;
    end
    
    %Introduce a very small noice to avoid equal samples (Probably for our purpose it's not usefull)
    x=x + abs(randn(1))* exp(-10);
    y= y + abs(randn(1)) * exp(-10);
    
    %Standardizing observations to unit variance and zero mean
    x = bsxfun( @rdivide , bsxfun(@minus,x,mean(x)) , std(x));
    y = bsxfun( @rdivide , bsxfun(@minus,y,mean(y)) , std(y));
    
    %Computing distances to kth neightbour on x,y and z plane
    z=[x,y];
    dz=dx+dy;
    
    
    Hx = EntropyLocalGaussian(x , Optim);
    Hy = EntropyLocalGaussian(y , Optim);
    Hz = EntropyLocalGaussian(z , Optim);
    
    MI = Hx + Hy - Hz;
end


function [ Entropy ] = EntropyLocalGaussian( x , Optim)
    [ sizex , dx ] = size(x);
    H=zeros(dx,dx);
    k=4;
    Entropy=0;
    
    %Evaluate Bandwidth as the average of pairwise k-th nearest neighbour in
    for i=1:dx
        [ ~ , tmp ] = knnsearch(x(:,i), x(:,i) , 'k',k+1);
        tmp = tmp(:,k+1);
        %diagonal bandwidth matrix
        H(i,i)=sum(tmp)/sizex;
    end
    
    [ index , dist ] = knnsearch(x,x,'k',k+1);
    index(:,1) = [];
    dist = dist(:,k+1);
    
    
    %Indexes for inlining parameters in fminunc
    i1 =1;
    s1 = dx;
    i2 = s1+1;
    s2 = i2 + (dx*dx)-1;
    
    %start from c?
    c = cov(x);
    
    % Guarantee that c is really symmetric
    c = (c + c')/2;
    %Cholesky parametrization to ensure semi-definite matrix.
    %L= cholcov(c);
    L=cholcov(H);
    
    %Evaluate Likelihood function for mean and variance at each sample point.
    % xi - sample for which we are evaluating likelihood
    % x0 - starting point
    % in - inlined parameter to optimize, covariance matrix & mean

   % options = optimoptions(@fminunc,'Display','iter','Algorithm','quasi-newton');
    options = optimoptions(@fminunc,'Algorithm','quasi-newton','Display', 'off');
   dia= diag(ones(1,dx)); 
   for i=1:sizex
        %Starting point for optimization of mean & covariance
        
        % Select d-dimensional point
        xin = x(i,:);
        xlist=x(index(i,:),:);
        %x0= [ xin , L(:)'];
         x0 = [ mean(x) , dia(:)' ];
        if Optim
            %Objective function to optimize
            fun = @(in)objfun( xin , xlist ,  in(i1:s1) , reshape( in(i2:s2 ), dx,dx), H );
            % Solve Optimization problem
            res = fminunc( fun , x0 ,options );
            mu = res(i1:s1);
            L_res = reshape(res(i2:s2),dx,dx);
            sigma = transpose(L_res) * L_res;
        else
            
            mu = xin;
            cc = cov([xin;xlist]);
            fin = cholcov(cc,0);
            sigma = transpose(fin) * fin;
        end
        
        % Entropy += log(f(x_i))
        % f(x_i)= d_dimensional_Gaussian(x_i,mu(x_i),sigma(x_i))
        % Place a Gaussian at each sample point
        
        Entropy = Entropy + log( mvnpdf(xin,mu,sigma) );
    end
    
    Entropy = -Entropy/sizex;
    
    %options = optimoptions(@fminunc,'Algorithm','quasi-newton');
    %options = optimoptions(@fminunc,'Display','iter','Algorithm','quasi-newton','SpecifyObjectiveGradient',true);
    %   options = optimset(@fminunc);
    %   x0 = randn(1,1000);
    %   H = fminunc(@objfun , x0 , options);
    
end

% xi point for which parameter has to be calculcated
% x list of samples
% mu, sigma mean and covariance to be maximized
% H bandwidth matrix
function f = objfun( xi , x,  mu ,  L  , H )
  %too speed up, remove controls
    %{
    [ sizex, dx ] = size(x);
    [sizexi, dxi] =size(xi);
    
    if(sizexi ~= 1)
        error('First parameter has to be a d-dimensional point');
    end
    if (dx ~= dxi)
        error('the points dimensionality in the first two arguments has to be the same');
    end
   %}
    
    %Guarantee symmetry
    %sigma = (sigma + sigma')/2;
    %[V,D] = eig(sigma);
    %V1 = V(:,1);
    %sigma = sigma + V1*V1'*(eps(D(1,1))-D(1,1));
    
    sigma = transpose(L) * L;
    popo = sum( mvnpdf(x, xi,H) .* log(mvnpdf(x, mu,  sigma ) )  );
    f=-( popo/length(x) ) +  mvnpdf(xi , mu , H+sigma);
    
end







