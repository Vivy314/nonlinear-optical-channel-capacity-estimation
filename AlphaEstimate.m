function [ alpha ] = AlphaEstimate( e_x, e_y ,dx ,dy,k , N )
    %Evaluate array alpha, with size Nx1, one element per sampled point
    %For each elemet repeat the calculation @Repeat times.
    
   
    Repeat  = 8;
    V= bsxfun(@times , power(  e_x, dx ) , power(e_y, dy ));
    %       V = bsxfun(@times , power( e_x , dx ) ,power( e_y ,dy ) );
    
    fin = zeros(N,1); 
    tmp=zeros(Repeat,1);
    
    for j=1:N
        for i=1:Repeat
            V1=1;
            %generate k 2dx-dimensional points inside the volume of sample j
            a_z = [e_x(j) * rand(k, dx) ,e_y(j)* rand(k, dy)]; 
            %Compute their PCA
            [~,pca_z] = pca(a_z,'NumComponents',dx+dy);
            %Evaluate the volume
            for u=1:numel(pca_z)/length(pca_z)
                V1 = V1 .* max( abs( pca_z(:,u) -min(pca_z(:,u))));
            %Checked that as below, wrong value    
            %     V1 = V1 .* max( abs( pca_z(:,u)+exp(-10) ));
            end
            % Ratio among the original volume and the PCA
            tmp(i) =  bsxfun(@rdivide , V1 , V(j));
        end
        %Correction factor for sample j
        fin(j)=mean(tmp);
    end
    %old
    %fin = sort(fin);
    %alpha = fin(floor(epsilon*N));
    % try
    alpha = fin;
    
    
    %=====================================================================
 %{   
    Repeat  = 5*10^5;
    epsilon = 5*10^(-3);
  
    tmp=zeros(Repeat,1);
    parfor j=1:Repeat
        V1=1;
        a_z = [ rand(k, dx) ,rand(k, dy)]; 
        [~,pca_z] = pca(a_z);    
            for u=1:(dx+dy)-1
                 V1 = V1 .* max( abs( pca_z(:,u)+exp(-10) ));
            end        
            tmp(j)=V1;
    end
    tmp= sort(tmp);
    alpha = tmp(ceil( Repeat*epsilon ));
    
    %Result > 0.0018 with 5*10^5 epsilon 5*10^-3
   %} 
end

