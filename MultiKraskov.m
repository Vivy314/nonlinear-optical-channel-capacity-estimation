function [ MI ,tmp ] = kNN_Kraskov( x , y , u, k , Mode, CorrectionTerm )
tmp=1;

%Checking Input
if nargin < 3 || nargin > 6
    error('Wrong input number.');
end
if nargin == 4
    Mode = 'Cube';
end
if nargin < 6
    CorrectionTerm = false;
end

[sizex,dx] = size(x);
[sizey,dy] = size(y);
[sizeu,du] = size(u);

if ( ~strcmp( Mode , 'Cube' ) && ~strcmp( Mode , 'Rectangle') )
    error('\"%s\" is not a valid mode for the method',Mode);
end
if ~isequal(sizex,sizey)|| ~isequal(dx,dy)
    error('Input/output require equal dimensionalities')
elseif (k ~= round(k) || k<= 0 )
    error('K parameter has to be a positive natural number');
end
N=sizex(1);
if k > N
    error('k parameter grater then sample size')
end

% Structuring complex numbers
if ~isreal(x)
    x= [ real(x),imag(x) ];
    dx=2*dx;
end
if ~isreal(y)
    y = [ real(y), imag(y)];
    dy=dy*2;
end


if ~isreal(u)
    u = [ real(u), imag(u)];
    du=du*2;
end
%Introduce a very small noice to avoid equal samples (Probably for our purpose it's not usefull)
x=x + abs(randn(1))* exp(-10);
y= y + abs(randn(1)) * exp(-10);

%Standardizing observations to unit variance and zero mean
x = bsxfun( @rdivide , bsxfun(@minus,x,mean(x)) , std(x));
y = bsxfun( @rdivide , bsxfun(@minus,y,mean(y)) , std(y));
u = bsxfun( @rdivide , bsxfun(@minus,u,mean(u)) , std(u));

%Computing distances to kth neightbour on x,y and z plane
z=[x,y,u];
dz=dx+dy+du;

%Anonymous Function defining the distance as:
%   ||z-z_i|| = max(|| x-x_i || , || y-y_i ||) where the inner norms
%   are arbitrary

%  DistanceMetric = @(X,Y)(max(transpose(pdist2(X(:,1:dx),Y(:,1:dx),'euclidean')),transpose(pdist2(X(:,dx+1:2*dx),Y(:,dx+1:2*dx),'euclidean') )));
DistanceMetric = @(X,Y)( max( max( transpose(pdist2(X(1:dx),Y(:,1:dx),'euclidean')), transpose(pdist2(X(dy+1:2*dy),Y(:,dy+1:2*dy),'euclidean') ) ) ,transpose(pdist2(X(2*dy+1:3*dy),Y(:,2*dy+1:3*dy),'euclidean')) )) ;

[points,e] = knnsearch(z,z,'k',k+1,'Distance','chebychev');

e=e(:, k+1 );
correctionTerm =points(:,2:k+1);
points=points(:,k+1);
n_x=zeros(N,1);
n_y=zeros(N,1);
n_u=zeros(N,1);

%Assuming hyper-cubic volume arround each point
if (strcmp(Mode,'Cube'))
    e_x=e;
    e_y=e;
    e_u=e;
    
    for j=1:N
        n_x(j) = sum( pdist2(x,x(j,:),'euclidean') < e(j) )-1;
        n_y(j) = sum ( pdist2(y,y(j,:),'euclidean') < e(j) ) -1;
        n_u(j) = sum ( pdist2(u,u(j,:),'euclidean') < e(j) ) -1;
        %  tmp1%HERE IS THE PROBLEM
        %n_x(j)= sum(tmp1)-1;% -1 cause i do not have to count the centered point
        %n_y(j)= sum(tmp2)-1;
    end
    
    Sum = ( sum(psi(n_x))+sum(psi(n_y) +sum(psi( n_u) ) ) )/N;
    MI=2*psi(N)+psi(k)-Sum;
    
    %Assuming hyper-rectangle volume arround each point
elseif (strcmp(Mode,'Rectangle'))
    %without points=points(:,k+1)
    %e_x = diag( pdist2(x ,  x( points(:,k+1),: ) , 'euclidean'));
    
    %e_y= diag( pdist2(y , y( points(:,k+1) , : ) , 'euclidean') );
    e_x = diag( pdist2(x ,  x( points ,: ) , 'euclidean'));
    e_y= diag( pdist2(y , y( points , : ) , 'euclidean') );
    for j=1:N
        n_x(j) = sum ( pdist2(x,x(j,:),'euclidean')<= e_x(j) )-1 ;
        n_y(j) = sum ( pdist2(y,y(j,:),'euclidean')<= e_y(j) )-1;
    end
    
    Sum=sum(psi(n_x)+psi(n_y));
    MI=psi(N)+psi(k)-(1/k)-Sum*(1/N);
end






%

if CorrectionTerm
    %1) Compute PCA of k-neighbour for each observation
    % I think...get a vector with the k-neighbour of x_i >>
    %            compute forall neigh x_i+(x_j-mean(neighbour)) to center the data in x_i (THe volume do not change.. stupid.. think about it) >>
    %           pca(neighbour) >> prudct among maximums of each principal components.
    
    %RELATED TO x_i (element of y)
    V1=ones(N,1);
    V=ones(N,1);
    for i=1:N
        neighbour=z(correctionTerm(i,:),:);
        %        neighbour(i)=z(i)+(neighbour(i)-mean) did not get the impact of
        %        cenetering the volum in x_i
        [~,new_z] = princomp(neighbour);
        
        
        %2) Compute the hyper-rectangle including the PCA
        for j=1:(dx+dy)-1
            %V1(i) = V(i) * max( new_z(:,j))-min(new_z(:,j)); Secondo me
            
            % Introduce small variation to eliminate zero volumes, ask
            % prof
            
            V1(i) = V1(i) * max( abs( new_z(:,j)+exp(-10) -min(new_z(:,j)))); %paper
            
        end
        
        if strcmp(Mode,'Rectangle')
            V(i)=e_x(i)^dx*e_y(i)^dy;
            %V(i)=e_x(i)*e_y(i);
 
        end
        
        if strcmp( Mode , 'Cube' )
            V(i)= bsxfun(@times , power( e(i), dx ) , power(e(i), dy ));
            %V(i) = e(i)^2;
        end
    end
    %alpha = AlphaEstimate(e_x,e_y,dx,dy, k , N);
    tmp = bsxfun(@rdivide , V1 , V);
    %if tmp<5
        out = 1/N * sum( log(tmp) );
    %else out = 0;
    %end
    
  	
    
    MI=MI-out;
    
    
end

%figure(1)
%[min(x)-1, max(x)+1, min(y)-1, max(y)+1]
%axis([min(x)-1, max(x)+1, min(y)-1, max(y)+1])
%{
for i=1:N
    text(x(i),y(i),num2str(i))
    h= rectangle('Position',[x(i)-e(i),y(i)-e(i),2*e(i),2*e(i)]);
    set(h,'EdgeColor',[mod(i ,2 ) 1-mod(i,2) 0]);
end
%}

%{
    closer_idx=zeros(N,k+1);
    min_distance=zeros(N,k+1);
    for j=1:N
            io =  max( pdist2(x, x(j,1:dx), 'euclidean' ) , pdist2(y, y(j,1:dx), 'euclidean' ));
         for i=1:k+1
            [~, closer_idx(j,i)] = min(io);
            min_distance(j,i)=io(closer_idx(j,i));
            io(closer_idx(j,i))=[];
        end
    end
    points=closer_idx;
    e=min_distance;
%}

end