function [ MI ,tmp ] = kNN_Kraskov( x , y , k , Mode, CorrectionTerm )
    tmp=1;
    
    %Checking Input
    if nargin < 3 || nargin > 6
        error('Wrong input number.');
    end
    if nargin == 3
        Mode = 'Cube';
    end
    if nargin < 5
        CorrectionTerm = false;
    end
    
    [sizex,dx] = size(x);
    [sizey,dy] = size(y);
    
    if ( ~strcmp( Mode , 'Cube' ) && ~strcmp( Mode , 'Rectangle') )
        error('\"%s\" is not a valid mode for the method',Mode);
    end
    
    if ~isequal(sizex,sizey)|| ~isequal(dx,dy)
        error('Input/output require equal dimensionalities')
    elseif (k ~= round(k) || k<= 0 )
        error('K parameter has to be a positive natural number');
    end
    
    if k > sizex
        error('k parameter grater then sample size')
    end
    
    % Structuring complex numbers
    if ~isreal(x)
        x= [ real(x),imag(x) ];
        dx=2*dx;
    end
    if ~isreal(y)
        y = [ real(y), imag(y)];
        dy=dy*2;
    end
    
    %Introduce a very small noice to avoid equal samples (Probably for our purpose it's not usefull)
    x=x + abs(randn(1))* exp(-10);
    y= y + abs(randn(1)) * exp(-10);
    
    %Standardizing observations to unit variance and zero mean
    x = bsxfun( @rdivide , bsxfun(@minus,x,mean(x)) , std(x));
    y = bsxfun( @rdivide , bsxfun(@minus,y,mean(y)) , std(y));
    
    %Computing distances to kth neightbour on x,y and z plane
    z=[x,y];
    dz=dx+dy;
    
    %Anonymous Function defining the distance as:
    %   ||z-z_i|| = max(|| x-x_i || , || y-y_i ||) where the inner norms
    %   are arbitrary
    
    DistanceMetric = @(X,Y)( max(transpose(pdist2(X(1:dx),Y(:,1:dx),'euclidean')),transpose(pdist2(X(dy+1:2*dy),Y(:,dy+1:2*dy),'euclidean') )));
    
    [points,e] = knnsearch(z,z,'k',k+1,'Distance',DistanceMetric);
    %Select just the k-th distances
    e=e(:, k+1 );
    correctionTerm =points(:,2:k+1);
    points=points(:,k+1);
    
    n_x=zeros(sizex,1);
    n_y=zeros(sizex,1);
    
    %Assuming hyper-cubic volume arround each point
    if (strcmp(Mode,'Cube'))
        e_x=e;
        e_y=e;
        
        for j=1:sizex
            n_x(j) = sum( pdist2(x,x(j,:),'euclidean') < e(j) )-1;
            n_y(j) = sum ( pdist2(y,y(j,:),'euclidean') < e(j) ) -1;
        end
        
        Sum = ( sum(psi(n_x+1))+sum(psi(n_y+1)) )/sizex;
        MI=psi(sizex)+psi(k)-Sum;
        
    %Assuming hyper-rectangle volume arround each point
    elseif (strcmp(Mode,'Rectangle'))
        
        %Evaluate the k-th distance projected in the x/y plane
        e_x = diag( pdist2(x ,  x( points ,: ) , 'euclidean'));
        e_y= diag( pdist2(y , y( points , : ) , 'euclidean') );
        
        for j=1:sizex
            n_x(j) = sum ( pdist2(x,x(j,:),'euclidean')<= e_x(j) ) ;
            n_y(j) = sum ( pdist2(y,y(j,:),'euclidean')<= e_y(j) );
        end
        
        Sum=( sum(psi(n_x))+sum(psi(n_y) ) )/sizex;
        MI=psi(sizex)+psi(k)-(1/k)-Sum;
        
    end
    
    
    if CorrectionTerm
        % Heuristically evaluate the discrimination factor alpha
        alpha = AlphaEstimate(e_x,e_y,dx,dy, k , sizex);
        
        %1) Compute PCA of k-neighbour for each observation
        
        V1=ones(sizex,1);
        V=ones(sizex,1);
        out = zeros(sizex,1);
        
        for i=1:sizex
            % for each sample i, get the set of its k neighbours
            neighbour=z(correctionTerm(i,:),:);
            %PCA on the k neighbour of sample i
            [~,new_z] = pca(neighbour);
            
            %2) Compute the hyper-rectangle volume taht include the PCA
            %Non sono convinto del motivo per cui l'ultima e' sempre 0
            for j=1:numel(new_z)/length(new_z)
                V1(i) = V1(i) * max( abs( new_z(:,j)+exp(-10) -min(new_z(:,j))));
               % V1(i) = V1(i) * max( abs( new_z(:,j)+exp(-10)));
            end
            %Evaluate the Kraskov volumes
            if strcmp(Mode,'Rectangle')
                V(i)=(e_x(i))^dx*(e_y(i))^dy;
            end
            
            if strcmp( Mode , 'Cube' )
                V(i)= bsxfun(@times , power( e(i), dx ) , power(e(i), dy ));
            end
            
            if V1(i)/V(i) < alpha(i)
                out(i) = log(V1(i)/V(i)) ;
            end
        end
        
        MI=MI- sum(out)/sizex;
        
    end
    
   
 %Code to scatters samples and plot Kraskov's Volumes   
    
    %figure(1)
    %[min(x)-1, max(x)+1, min(y)-1, max(y)+1]
    %axis([min(x)-1, max(x)+1, min(y)-1, max(y)+1])
    %{
for i=1:sizex
    text(x(i),y(i),num2str(i))
    h= rectangle('Position',[x(i)-e(i),y(i)-e(i),2*e(i),2*e(i)]);
    set(h,'EdgeColor',[mod(i ,2 ) 1-mod(i,2) 0]);
end
    %}
    
    %{
    closer_idx=zeros(sizex,k+1);
    min_distance=zeros(sizex,k+1);
    for j=1:sizex
            io =  max( pdist2(x, x(j,1:dx), 'euclidean' ) , pdist2(y, y(j,1:dx), 'euclidean' ));
         for i=1:k+1
            [~, closer_idx(j,i)] = min(io);
            min_distance(j,i)=io(closer_idx(j,i));
            io(closer_idx(j,i))=[];
        end
    end
    points=closer_idx;
    e=min_distance;
    %}
    
end