function MI = mutual_information(x,y,id)
%MI=mutual_information(x,y,id) computes the mutual information between x and y
%by means of the average nearest neighbor distance
%
%   x:  sequence of N random variables of dimension dx
%   y:  sequence of N random variables of dimension dy
%   id: distance metric (0=chebichev,else=euclidean)
%
%   The mutual information MI (in nats) is obtained by estimating the
%   differential entropies of the x and y variables and then calculating
%   I(X,Y)=H(X)+H(Y)-H(X,Y)


[Nx,dx]=size(x);
[Ny,dy]=size(y);
%[Nu,du]=size(u);
if ne(Nx,Ny)
    error('Lengths of input and output sequences are different!');
else
    N=Nx;
end
%{
if ne(Nx,Nu)
    error('Lengths of input and output sequences are different!');
else
    N=Nx;
end
%}
irx=isreal(x);
iry=isreal(y);
%ifru=isreal(u);

if irx
    zx=x;
else
    zx = [real(x),imag(x)];
    dx=2*dx;
end
if iry
    zy=y;
else
    zy = [real(y),imag(y)];
    dy=2*dy;
end

%{
if iru
    zu=u;
else
    zu = [real(u),imag(u)];
    du=2*du;
end
%}
z  = [zx,zy];%,zu];
dz=dx+dy;%+du;
if (id==0)
    distance='chebychev';
    cdx=1;
    cdy=1;
%    cdu=1;
    cdz=1;
    % elseif (id==1)
    %     distance='cityblock';
    %     cdx=1;
    %     cdy=1;
    %     cdz=1;
else
    distance='euclidean';
    cdx=pi^(0.5*dx)/gamma(1+0.5*dx)/2^dx;
    cdy=pi^(0.5*dy)/gamma(1+0.5*dy)/2^dy;
%    cdu=pi^(0.5*du)/gamma(1+0.5*dy)/2^du;
    cdz=pi^(0.5*dz)/gamma(1+0.5*dz)/2^dz;
end

[~,distx] = knnsearch(zx,zx,'k',2,'distance',distance);
[~,disty] = knnsearch(zy,zy,'k',2,'distance',distance);
%[~,distu] = knnsearch(zu,zu,'k',2,'distance',distance);
[~,distz] = knnsearch(z,z,'k',2,'distance',distance);
Hx = -psi(1)+psi(N)+log(cdx)+dx*mean(log(2*distx(:,2)));
Hy = -psi(1)+psi(N)+log(cdy)+dy*mean(log(2*disty(:,2)));
Hxy = -psi(1)+psi(N)+log(cdz)+dz*mean(log(2*distz(:,2)));
MI=Hx+Hy-Hxy;

end

